REMOTEHOST := root@las2.bounce
IMAGENAME := sneak/dockerized-workstation

default: remote-build

build:
	docker build -t $(IMAGENAME) .

upload:
	rsync -avP --delete ./ $(REMOTEHOST):dockerized-workstation/

remote-build: upload
	ssh -t $(REMOTEHOST) "cd dockerized-workstation && make build"

tryout:
	ssh -t $(REMOTEHOST) "docker run -ti $(IMAGENAME)"
